#! /usr/bin/env python
# -*- coding: utf-8 -*-


import json
import os
import psycopg2
import psycopg2.extras


connection = psycopg2.connect(database = "udd", user = "public-udd-mirror", password = "public-udd-mirror", host = "public-udd-mirror.xvm.mit.edu")
cursor = connection.cursor(cursor_factory = psycopg2.extras.DictCursor)
cursor.execute("SELECT * from sources_uniq LIMIT 100")
rows = cursor.fetchall()
cursor.close()
connection.close()

if not os.path.exists('data'):
    os.mkdir('data')

for row in rows:
    row = dict(row.items())
    with open ('data/{}.json'.format(row['source']), 'w') as file:
        json.dump(row, file, encoding = "utf-8", ensure_ascii = False, indent = 2)
    # print json.dumps(row, encoding = "utf-8", ensure_ascii = False, indent = 2)

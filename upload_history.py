#! /usr/bin/env python
# -*- coding: utf-8 -*-


import json
import os
import psycopg2
import psycopg2.extras

#connection = psycopg2.connect(database = "udd", user = "public-udd-mirror", password = "public-udd-mirror", host = "public-udd-mirror.xvm.mit.edu")
connection = psycopg2.connect(database = "udd", user = "udd", password = "udd", host = "localhost")
cursor = connection.cursor(cursor_factory = psycopg2.extras.DictCursor)
request = """
SELECT sources_uniq.*, up1.max1
FROM sources_uniq left join (SELECT source, MAX(date) as max1 from upload_history GROUP BY source) AS up1
ON sources_uniq.source = up1.source
LIMIT 10
"""
# request = """
# SELECT sources_uniq.*, upload_history.*
# FROM sources_uniq left join upload_history
# ON sources_uniq.source = upload_history.source
# LIMIT 10
# """
cursor.execute(request)
rows = cursor.fetchall()
cursor.close()
connection.close()

for row in rows:
    row =  dict(row.items())
    print row
    row['max1'] = row['max1'].isoformat()
    print json.dumps(row, encoding = "utf-8", ensure_ascii = False, indent = 2)

# print rows
